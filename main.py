import sys
import time
import queue
import collections

import sounddevice
import numpy as np
import moderngl
import moderngl_window as mglw

VERTICES = np.array([-1.0, 1.0, 1.0, 1.0, 1.0, -1.0, -1.0, -1.0 ], dtype='f4')
INITIAL_DIMENSIONS = (800, 600)

def die(msg):
  print(msg)
  sys.exit(1)

class Example(mglw.WindowConfig):
  gl_version = (3, 3)
  window_size = INITIAL_DIMENSIONS
  aspect_ratio = None
  title = "float_me"

  def __init__(self, **kwargs):
    super().__init__(**kwargs)

    self.vertices = self.ctx.buffer(VERTICES)
    self.dimensions = INITIAL_DIMENSIONS
    self.sample_rate = 96000
    self.sample_size = 1600
    self.fft_size = 2**14
    self.pad_size = self.fft_size - self.sample_size

    self.spectrogram = {}
    self.spectrogram['program'] = self.fragment_program(open('spectrogram.glsl').read())
    self.spectrogram['vao'] = self.ctx.vertex_array(
      self.spectrogram['program'], [(self.vertices, '2f4', 'vertex')]
    )
    self.resize_fft(self.fft_size>>1)

    self.terrain = {}
    self.terrain['program'] = self.fragment_program(open('terrain.glsl').read())
    self.terrain['vao'] = self.ctx.vertex_array(
      self.terrain['program'], [(self.vertices, '2f4', 'vertex')]
    )
    self.terrain['program']['tex_src'] = 0
    self.terrain['program']['yaw'] = 3*np.pi/4
    self.terrain['program']['pitch'] = 0.35*np.pi
    self.terrain['program']['eye_distance'] = 250.0

    self.stream = sounddevice.InputStream(samplerate=self.sample_rate,
      blocksize=self.sample_size, device='pulse', channels=1, dtype='f4',
      latency='low', callback=self.callback, dither_off=True, clip_off=False
    )
    self.reset_stream()

    self.data_queue = queue.Queue(maxsize=1)
    self.last_status_time = 0.0
    self.last_callback_time = 0.0

    self.average_history_size = 100
    self.dt_callback_deque = collections.deque(maxlen=self.average_history_size)
    self.T_sampling_deque = collections.deque(maxlen=self.average_history_size)
    self.dt_frame_deque = collections.deque(maxlen=self.average_history_size)
    self.mouse_button_states = [False, False, False]

  def callback(self, data, frames, time_info, status):
    self.dt_callback_deque.append(time_info.currentTime - self.last_callback_time)
    self.T_sampling_deque.append(time_info.currentTime - time_info.inputBufferAdcTime)
    self.last_callback_time = time_info.currentTime

    start = time.process_time()
    data = data.T[0]
    data = np.pad(data, (0, self.pad_size))
    magnitudes = abs(np.fft.rfft(data)[1:])
    try:
      self.data_queue.put(magnitudes.copy(), block=False)
    except queue.Full:
      print('queue overflow, dropping incoming sample block')

  def reset_stream(self):
    if self.stream:
      print('resetting stream')
      self.stream.abort()

    self.stream.start()

  def fragment_program(self, fragment_shader):
    return self.ctx.program(
      vertex_shader=open('vertex.glsl').read(),
      fragment_shader=fragment_shader
     )

  def render(self, time, frame_time):
    if time - self.last_status_time > 1.0:
      self.last_status_time = time
      m_dtc= sum(self.dt_callback_deque)/self.average_history_size
      m_Ts = sum(self.T_sampling_deque)/self.average_history_size
      m_dtf = sum(self.dt_frame_deque)/self.average_history_size
      print(f"dt_callback={m_dtc:.5f} Ts={m_Ts:.5f} dt_frame={m_dtf:.5f}", end=' ')
      print(f"qsize={self.data_queue.qsize()}")

    self.dt_frame_deque.append(frame_time)

    if 'mouse_button' in self.spectrogram['program']:
      self.spectrogram['program']['mouse_button'] = tuple(self.mouse_button_states)

    magnitudes = self.data_queue.get()
    self.spectrogram['tex_fft'].write(magnitudes.astype('f4').tobytes())
    self.ctx.clear(1.0, 1.0, 1.0)
    self.spectrogram['fbo'].use()
    self.spectrogram['vao'].render(moderngl.TRIANGLE_FAN)
    self.ctx.copy_framebuffer(self.spectrogram['tex_src'], self.spectrogram['fbo'])
    #self.ctx.copy_framebuffer(self.ctx.screen, self.spectrogram['fbo'])
    self.ctx.screen.use()
    self.terrain['vao'].render(moderngl.TRIANGLE_FAN)

  def resize_spectrogram(self):
    width, height = self.dimensions
    self.spectrogram['tex_src'] = self.ctx.texture((width, height), 4, dtype='f4')
    self.spectrogram['tex_dst'] = self.ctx.texture((width, height), 4, dtype='f4')
    self.spectrogram['tex_src'].use(location=0)
    self.spectrogram['fbo'] = self.ctx.framebuffer([self.spectrogram['tex_dst']])

  def resize_fft(self, N):
    self.spectrogram['tex_fft'] = self.ctx.texture((N, 1), 1, dtype='f4')
    self.spectrogram['program']['tex_fft'] = 1
    self.spectrogram['tex_fft'].use(1)

  def resize(self, width, height):
    self.dimensions = (width, height)
    self.resize_spectrogram()

  def mouse_position_event(self, x, y, dx, dy):
    if 'mouse' in self.spectrogram['program']:
      self.spectrogram['program']['mouse'] = (x, self.dimensions[1] - y)
  def mouse_drag_event(self, x, y, dx, dy):
    if 'mouse' in self.spectrogram['program']:
      self.spectrogram['program']['mouse'] = (x, self.dimensions[1] - y)
    self.terrain['program']['yaw'].value += dx / 100;
    self.terrain['program']['pitch'].value -= dy / 100;
  def mouse_scroll_event(self, x_offset: float, y_offset: float):
    self.terrain['program']['eye_distance'].value += 5*y_offset
  def mouse_press_event(self, x, y, button):
    self.mouse_button_states[button - 1] = True
  def mouse_release_event(self, x: int, y: int, button: int):
    self.mouse_button_states[button - 1] = False

if __name__ == '__main__':
  Example.run()
