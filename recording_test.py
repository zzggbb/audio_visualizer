import sys
import queue
import sounddevice
import numpy
numpy.set_printoptions(
  threshold=sys.maxsize,
  linewidth=sys.maxsize
)

last_time = 0

q = queue.Queue()
debug = True

def callback(data, frames, time, status):
  q.put(data.T[0].copy())

  if debug:
    global last_time
    if status:
      print('***', status, '***')

    dt = time.currentTime - last_time
    if dt == 0.0:
      dt = 0.0000000001
    data = data.T
    _, length = data.shape
    print(length, "{:.8f}".format(dt), length / dt, data[0][0], data[0][-1])
    last_time = time.currentTime

stream = sounddevice.InputStream(
  samplerate=64000, blocksize=800,
  device='pulse', channels=1, dtype='float32',
  callback=callback, dither_off=True, clip_off=True)

while True:
  pass
