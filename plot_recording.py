import sys
from matplotlib import pyplot as plt

def get_data(lines, line_number):
  line = lines[line_number - 1][1:-2]
  data = [float(x) for x in line.split()]
  return data

with open('recording.log') as f:
  lines = f.readlines()
  data = [*get_data(lines, 309), *get_data(lines, 310)]
  plt.plot(data)
  plt.show()
