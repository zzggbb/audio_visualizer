#version 330
#define PI 3.14159265

uniform ivec2 mouse;
uniform bvec3 mouse_button;
uniform sampler2D tex_src;
uniform sampler2D tex_fft;
out vec4 color;

float linscale(float x, float x1, float x2, float y1, float y2) {
  return (y2-y1)/(x2-x1) * (x-x2) + y2;
}
float logyscale(float x, float x1, float x2, float y1, float y2) {
  return pow(2.0, log2((y2+1.0)/(y1+1.0))*(x-x2)/(x2-x1) + log2(y2+1.0)) - 1.0;
}
vec3 color_gradient(float t) {
  t *= 0.75;
  vec3 a = vec3(0.5);
  vec3 b = vec3(0.5);
  vec3 c = vec3(1.0);
  vec3 d = vec3(0.0, 0.33, 0.67);
  return sqrt(t)*(a + b * cos(2.0*PI*(c*t + d)));
}

float inv_smoothstep(float x) {
  return (x<0.5) ? sqrt(0.5*x) : 1.0-sqrt(0.5-0.5*x);
}

void main() {
  ivec2 src_dimensions = textureSize(tex_src, 0);
  float fft_N = textureSize(tex_fft, 0).x;
  vec2 uv = gl_FragCoord.xy / src_dimensions.xy;

  color = vec4(vec3(0.0), 1.0);
  int speed = 5;
  if (int(gl_FragCoord.y) < speed) {
    float fft_x = logyscale(uv.x, 0.0, 1.0, 0.0, fft_N-1.0);

    float fft_x1 = floor(fft_x);
    float fft_y1 = texelFetch(tex_fft, ivec2(fft_x1, 0), 0).x;

    float fft_x2 = ceil(fft_x);
    float fft_y2 = texelFetch(tex_fft, ivec2(fft_x2, 0), 0).x;

    float fft_y = linscale(fft_x, fft_x1, fft_x2, fft_y1, fft_y2);
    fft_y = fft_y * 50.0 / fft_N;
    //fft_y = inv_smoothstep(fft_y);
    color = vec4(vec3(fft_y), 1.0);
  } else {
    int y = int(floor(gl_FragCoord.y / speed))*speed - speed;
    color = texelFetch(tex_src, ivec2(floor(gl_FragCoord.x), y), 0);
  }
}
