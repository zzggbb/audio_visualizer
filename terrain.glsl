#version 330
#define PI 3.14159265
#define DT 0.5
#define BACKGROUND_COLOR vec3(0.05)
#define BOX vec3(200, 50, 200)
#define FLB vec3(-BOX.x/2.0, 0.0, -BOX.z/2.0)
#define BRT vec3(BOX.x/2.0, BOX.y, BOX.z/2.0)
#define BOX_X vec3(BOX.x, 0.0, 0.0)
#define BOX_Y vec3(0.0, BOX.y, 0.0)
#define BOX_Z vec3(0.0, 0.0, BOX.z)
#define SRC_DIMENSIONS textureSize(tex_src, 0)

uniform ivec2 mouse;
uniform bvec3 mouse_button;
uniform float yaw;
uniform float pitch;
uniform float eye_distance;
uniform sampler2D tex_src;
out vec4 color;

float linscale(float x, float x1, float x2, float y1, float y2) {
	return (y2-y1)/(x2-x1)*(x-x2) + y2;
}
vec2 screen_coord(vec2 xy, vec2 dim) {
  return (xy - 0.5*dim) / min(dim.x, dim.y);
}
vec3 sphere2xyz(float yaw, float pitch) {
  return vec3(sin(pitch)*cos(yaw), cos(pitch), sin(pitch)*sin(yaw));
}
mat3 lookat(vec3 eye, vec3 at, vec3 up) {
  vec3 f = normalize(at - eye);
  vec3 s = normalize(cross(f, up));
  vec3 u = normalize(cross(s, f));
  return mat3(s, u, -f);
}
float surface_discrete(vec2 c) {
  float x = linscale(c.x, -BOX.x*0.5, BOX.x*0.5, 0.0, SRC_DIMENSIONS.x - 1.0);
  float y = linscale(c.y, BOX.z*0.5, -BOX.z*0.5, 0.0, SRC_DIMENSIONS.y - 1.0);
  return texelFetch(tex_src, ivec2(x,y), 0).x;
}
vec3 light_model(vec3 eye, vec3 p) {
  return vec3(p.y / BOX.y);
}
vec4 line_quad_intersect(vec3 p0, vec3 s1, vec3 s2, vec3 r0, vec3 rd) {
	/*
	p0: corner of quadrilateral
	s1, s2: sides of quadrilateral, origin at p0
	r0: origin of line
	rd: direction of line

	return value:
		xyz: point of intersection (only valid if a is 1)
		a: 1 if intersection, 0 if no intersection
	*/

  // normal to surface of quadrilateral
  vec3 N = cross(s1, s2);
	float a = dot(N, rd);
  float b = dot(N, p0 - r0);

  if (a == 0.0)
    // line and quadrilateral are parallel
		return vec4(0);

  // line and quadrilateral are not parallel
  // distance from origin of line to point where line intersects the quadrilateral's plane
  float t = b / a;
  // point where line intersects the quadrilateral's plane
  vec3 p = r0 + t*rd;
  // scalar projection of p - p0 on s1
  float q1 = dot(p - p0, normalize(s1));
  float l1 = length(s1);
  if (q1 < 0.0 || q1 > l1) return vec4(0); // p is outside of quadrilateral

  // scalar projection of p - p0 on s2
  float q2 = dot(p - p0, normalize(s2));
  float l2 = length(s2);
  if (q2 < 0.0 || q2 > l2) return vec4(0); // p is outside of quadrilateral

  return vec4(p, 1);
}
#define UPDATE_INTERSECTIONS(p0, s1, s2, r0, rd) { \
    LQI = line_quad_intersect(p0, s1, s2, r0, rd); \
    if (LQI.a == 1.0) { \
        if (intersections == 0) p1 = LQI.xyz; \
        else p2 = LQI.xyz; \
        intersections++; \
    } \
}
vec3 render(vec3 eye, vec3 ray) {
  vec4 LQI;
  vec3 p1, p2;
  int intersections = 0;

  UPDATE_INTERSECTIONS(FLB, BOX_X, BOX_Y, eye, ray); // front
  UPDATE_INTERSECTIONS(FLB, BOX_Y, BOX_Z, eye, ray); // left
  UPDATE_INTERSECTIONS(FLB, BOX_X, BOX_Z, eye, ray); // bottom
  UPDATE_INTERSECTIONS(BRT, -BOX_X, -BOX_Y, eye, ray); // back
  UPDATE_INTERSECTIONS(BRT, -BOX_Y, -BOX_Z, eye, ray); // right
  UPDATE_INTERSECTIONS(BRT, -BOX_X, -BOX_Z, eye, ray); // top

  if (intersections == 0)
    // ray doesn't intersect with the bounding box
    return BACKGROUND_COLOR;

  // p_min is the closer intersection between the ray and the box
  // p_max is the further intersection between the ray and the box
  vec3 p_min, p_max;
  if (distance(p1, eye) > distance(p2, eye)) {
    p_min = p2;
    p_max = p1;
  } else {
    p_min = p1;
    p_max = p2;
  }

  float height;
  float height_last = 0.0;
  float py_last = 0.0;
  vec3 dp = DT*ray;
  bool p_min_below = p_min.y < surface_discrete(p_min.xz)*BOX.y;
  for (vec3 p = p_min; distance(p_min, p) < distance(p_min, p_max); p += dp) {
    height = surface_discrete(p.xz)*BOX.y;
    if (p.y > height && p_min_below || p.y < height && !p_min_below) {
      vec3 surface_point = p - dp + dp * (py_last - height_last)/(height - p.y + py_last - height_last);
      return light_model(eye, surface_point);
    }
    height_last = height;
    py_last = p.y;
  }

  // ray intersects with the bounding box but not the surface
  return BACKGROUND_COLOR;
}
void main() {
  vec2 p = screen_coord(gl_FragCoord.xy, SRC_DIMENSIONS.xy);
  //float yaw = linscale(?, ?, ?, -PI, PI);
  //float pitch = linscale(?, ?, ?, 0.0, PI);
  vec3 eye = clamp(eye_distance, 50.0, 1000.0) * sphere2xyz(yaw, pitch);
  vec3 at = vec3(0.0);
  vec3 up = vec3(0.0, 1.0, 0.0);
  vec3 ray = lookat(eye, at, up) * normalize(vec3(p, -1.0));
  color = vec4(render(eye, ray), 1.0);
  return;
  vec4 lqi = line_quad_intersect(
    vec3(-BOX.x*.5, 0.0, -BOX.z*0.5),
    vec3(BOX.x, 0.0, 0.0),
    vec3(0.0, 0.0, BOX.z),
    eye, ray
  );
  vec3 c = (lqi.a == 1.0) ? vec3(surface_discrete(lqi.xz)) : BACKGROUND_COLOR;
  color = vec4(c, 1.0);
}
